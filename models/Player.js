var mongoose = require('mongoose');
var RankManager = require('../modules/RankManager.js');
var Schema = mongoose.Schema;

if (!mongoose.connection.readyState) {
    mongoose.connect('mongodb://localhost:27017/windjammers-ranking');
}

var settings = RankManager.getSettings();
console.log(settings);

var PlayerSchema = new Schema({
    _id: {type: Schema.Types.ObjectId, auto: true, index: true},
    username: {type: String, index: true},
    challonge: {type: String, index: true},
    first_name: {type: String, default: ''},
    last_name: {type: String, default: ''},
    previous_skill: {type: Array, default: [settings.mu, settings.sigma]},
    skill: {type: Array, default: [settings.mu, settings.sigma]},
    previous_level: {type: Number, default: 0},
    level: {type: Number, default: 0},
    previous_elo: {type: Number, default: null},
    elo: {type: Number, default: settings.base_elo}
}, {autoIndex: false});

PlayerSchema.statics.findOneByChallonge = function (challonge, cb) {
    this.findOne({challonge: challonge}, cb);
};

PlayerSchema.virtual('full_name').get(function () {
    return this.first_name + ' ' + this.last_name;
});

PlayerSchema.pre('save', function (next) {
    this.previous_level = this.previous_skill[0] - (settings.divider * this.previous_skill[1]);
    this.level = this.skill[0] - (settings.divider * this.skill[1]);
    next();
});

module.exports = mongoose.model('Player', PlayerSchema);