var mongoose = require('mongoose');
var Schema = mongoose.Schema;

if(!mongoose.connection.readyState){
    mongoose.connect('mongodb://localhost:27017/windjammers-ranking');
}

var MatchSchema = new Schema({
    _id: {type: Schema.Types.ObjectId, auto: true, index: true},
    winner_id: {type: Schema.Types.ObjectId, auto: false, index: true, ref: 'Player'},
    loser_id: {type: Schema.Types.ObjectId, auto: false, index: true, ref: 'Player'},
    recorded_at: Date
}, {autoIndex: false});

module.exports = mongoose.model('Match', MatchSchema);