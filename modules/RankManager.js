function RankManager(mu, sigma, k, base_elo) {
    var defaults = {
        mu: 25,
        divider: 3,
        sigma: 25 / 3,
        k: 32,
        base_elo: 1500
    };
    defaults['mu'] = typeof mu !== 'undefined' ? mu : 25;
    defaults['divider'] = typeof sigma !== 'undefined' ? (sigma / defaults.mu) : 3;
    defaults['sigma'] = typeof sigma !== 'undefined' ? sigma : defaults.mu / defaults.divider;
    defaults['k'] = typeof k !== 'undefined' ? k : 32;
    defaults['base_elo'] = typeof base_elo !== 'undefined' ? base_elo : 1500;

    var settings = JSON.parse(JSON.stringify(defaults));

    var trueskill = require('trueskill');
    trueskill.SetInitialMu(defaults.mu);
    trueskill.SetInitialSigma(defaults.sigma);
    trueskill.SetParameters(null, null, 0, null); // 0 percent chance of draw

    var elo = require('elo-rank')(defaults.k);

    return {
        getSettings: function () {
            return settings;
        },
        adjustRanks: function (winner, loser) {
            var temp_w = {
                    skill: winner.skill,
                    rank: 1
                },
                temp_l = {
                    skill: loser.skill,
                    rank: 2
                };
            trueskill.AdjustPlayers([temp_w, temp_l]);

            winner.previous_skill = winner.skill;
            loser.previous_skill = loser.skill;

            winner.skill = temp_w.skill;
            loser.skill = temp_l.skill;

            winner.previous_elo = winner.elo;
            loser.previous_elo = loser.elo;

            var expected = {
                winner: elo.getExpected(winner.elo, loser.elo),
                loser: elo.getExpected(loser.elo, winner.elo)
            };

            winner.elo = elo.updateRating(expected.winner, 1, winner.elo);
            loser.elo = elo.updateRating(expected.loser, 0, loser.elo);
        }
    }
}

module.exports = new RankManager;