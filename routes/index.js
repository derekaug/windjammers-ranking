var express = require('express');
var router = express.Router();

var Player = require('../models/Player.js');
var Match = require('../models/Match.js');
var RankManager = require('../modules/RankManager.js');

var temp = new Player({
    challonge: 'test',
    username: 'test'
});

var temp2 = new Player({
    challonge: 'test2',
    username: 'test2'
});

RankManager.adjustRanks(temp, temp2);

temp.save();
temp2.save();

/* GET home page. */
router.get('/', function (req, res) {
    var players = [];
    /*
    var playerExists = function (username, players_arr) {
        var results = players_arr.filter(function (player) {
            return player.challonge_username === username;
        });
        return results.length > 0;
    };
    var getPlayerByName = function (username, players_arr) {
        var rvalue = null;
        var results = players_arr.filter(function (player) {
            return player.challonge_username === username;
        });
        if (results.length > 0) {
            rvalue = results[0];
        }
        return rvalue;
    };
    var getPlayerByID = function (id, players_arr) {
        var rvalue = null;
        var results = players_arr.filter(function (player) {
            return player.challonge_ids.indexOf(id) !== -1;
        });
        if (results.length > 0) {
            rvalue = results[0];
        }
        return rvalue;
    };
    var sortByLevel = function (players_arr) {
        players_arr.sort(function (a, b) {
            return b.level - a.level;
        });
        return players_arr;
    };
    var addPlayers = function (players_arr) {
        if (Array.isArray(players_arr)) {
            players_arr.forEach(function (object, index, array) {
                var player = object.participant;
                if (!playerExists(player.username, players)) {
                    players.push({
                        'challonge_ids': [player.id],
                        'challonge_username': player.username,
                        'level': 0,
                        'skill': [25.0, 25.0 / 3.0]
                    });
                }
                else {
                    var temp = getPlayerByName(player.username, players);
                    temp.challonge_ids.push(player.id);
                }
            });
        }
    };
    var updateSkills = function (matches_arr) {
        if (Array.isArray(matches_arr)) {
            matches_arr.forEach(function (object, index, array) {
                var match = object.match,
                    winner = null,
                    winner_ref = null,
                    loser = null,
                    loser_ref = null;

                if (match.winner_id) {
                    winner_ref = getPlayerByID(match.winner_id, players);
                    winner = {
                        skill: winner_ref.skill,
                        rank: 1
                    };
                }
                if (match.loser_id) {
                    loser_ref = getPlayerByID(match.loser_id, players);
                    loser = {
                        skill: loser_ref.skill,
                        rank: 2
                    };
                }

                if (winner !== null && loser !== null) {
                    trueskill.AdjustPlayers([winner, loser]);
                    winner_ref.preskill = winner_ref.skill;
                    loser_ref.preskill = loser_ref.skill;

                    winner_ref.skill = winner.skill;
                    loser_ref.skill = loser.skill;

                    winner_ref.level = rounded(winner_ref.skill[0] - (3 * winner_ref.skill[1]), 5);
                    loser_ref.level = rounded(loser_ref.skill[0] - (3 * loser_ref.skill[1]), 5);
                }
            });
        }
    };

    var rounded = function (number, decimals) {
        return number.toFixed(decimals);
    };

    var tourney = require('../data/tourney1.json');
    tourney = tourney.tournament;
    addPlayers(tourney.participants);
    updateSkills(tourney.matches);

    tourney = require('../data/tourney2.json');
    tourney = tourney.tournament;
    addPlayers(tourney.participants);
    updateSkills(tourney.matches);

    tourney = require('../data/tourney3.json');
    tourney = tourney.tournament;
    addPlayers(tourney.participants);
    updateSkills(tourney.matches);

    tourney = require('../data/tourney4.json');
    tourney = tourney.tournament;
    addPlayers(tourney.participants);
    updateSkills(tourney.matches);

    tourney = require('../data/tourney5.json');
    tourney = tourney.tournament;
    addPlayers(tourney.participants);
    updateSkills(tourney.matches);

    tourney = require('../data/tourney6.json');
    tourney = tourney.tournament;
    addPlayers(tourney.participants);
    updateSkills(tourney.matches);

    players = sortByLevel(players);

    res.render('index', {players: players});*/
});

module.exports = router;
